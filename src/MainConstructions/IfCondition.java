package MainConstructions;

import Lexer.LexemeType;
import Parser.ControlStructureInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 29.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class IfCondition extends ControlStructureInterface {
    public Boolean condition = false;
    public Integer start_line;
    public Integer end_line;

    public IfCondition(String expression, Integer start_line) {
        this.start_line = start_line;

        Pattern pattern_condition = LexemeType.CONDITION.getPattern();
        Matcher matcher = pattern_condition.matcher(expression);
        while (matcher.find()) {
            String value = matcher.group();
            Condition condition = new Condition(value);
            this.condition = condition.result;
        }
    }

    @Override
    public void logic() {

    }

    @Override
    public void setEndLine(Integer line) {
        this.end_line = line;
    }
}
