package MainConstructions;

import Lexer.LexemeType;
import Lexer.RightTokens;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 29.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class Condition {

    private String left_part;
    private String right_part;
    private String operation;
    private ArrayList<Variable> variables = new ArrayList<>();
    private ArrayList<String> digits = new ArrayList<>();
    public Boolean result = false;

    public Condition(String condition) {
        Pattern pattern_var = LexemeType.VAR.getPattern();
        Pattern pattern_digit = LexemeType.DIGIT.getPattern();

        Matcher matcher = pattern_var.matcher(condition);
        while (matcher.find()) {
            String var_name = matcher.group();
            Variable var = Variable.get(var_name);
            this.variables.add(var);
        }

        Pattern pattern_operation = Pattern.compile("(==|<=|>=|!=|<|>)");
        matcher = pattern_operation.matcher(condition);
        while (matcher.find()) {
            this.operation = matcher.group();
        }

        matcher = pattern_digit.matcher(condition);
        while (matcher.find()) {
            this.digits.add(matcher.group());
        }

        if ((this.digits.size() == 1 && this.variables.size() == 1
                || this.digits.size() == 2 && this.variables.size() == 0
                || this.variables.size() == 2 && this.digits.size() == 0) && this.operation.length() > 0) {

            this.logic();
        } else {
            throw new ExceptionInInitializerError("Condition error");
        }
    }

    private void logic() {

        if (this.digits.size() == 2) {
            this.left_part = this.digits.get(0);
            this.right_part = this.digits.get(1);
        }

        if (this.variables.size() == 2) {
            this.left_part = this.variables.get(0).value;
            this.right_part = this.variables.get(1).value;
        }

        if (this.variables.size() == 1 && digits.size() == 1) {
            this.left_part = this.variables.get(0).value;
            this.right_part = this.digits.get(0);
        }

        Boolean condition;
        switch (this.operation) {
            case "==":
                condition = Double.parseDouble(this.left_part) == Double.parseDouble(this.right_part);
                break;
            case "<=":
                condition = Double.parseDouble(this.left_part) <= Double.parseDouble(this.right_part);
                break;
            case ">=":
                condition = Double.parseDouble(this.left_part) >= Double.parseDouble(this.right_part);
                break;
            case "!=":
                condition = Double.parseDouble(this.left_part) != Double.parseDouble(this.right_part);
                break;
            case "<":
                condition = Double.parseDouble(this.left_part) < Double.parseDouble(this.right_part);
                break;
            case ">":
                condition = Double.parseDouble(this.left_part) > Double.parseDouble(this.right_part);
                break;
            default:
                condition = false;
        }

        this.result = condition;
    }
}
