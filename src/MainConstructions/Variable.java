package MainConstructions;

import HashMap.MyHashMap;
import Lexer.LexemeType;
import Lexer.RightTokens;
import Lexer.Token;
import Parser.ReversePolishEntry;

import java.util.ArrayList;

/**
 * Created by Anochin Nikolay on 29.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class Variable {
    public Integer visibilityArea;
    public String name;
    public String value;
    public MyHashMap<String> hashMap;
    public static Integer visibility = 0;

    public Variable(String name, ArrayList<Token> tokens) {
        this.name = name;
        this.visibilityArea = Variable.visibility;
        if (tokens.get(0).type.equals(LexemeType.FUNCTION.name())) {
            if (tokens.get(0).value.equals("createMap()")) {
                this.hashMap = new MyHashMap<>();
            }
            this.value = tokens.get(0).value;
        } else if (tokens.get(0).type.equals(LexemeType.FUNCTION_WITH_LONG_VARS.name())) {

        } else {
            this.value = this.calculateValue(tokens);
        }
        String key = name + "_" + Integer.toString(this.visibilityArea);

        for (int i = Variable.visibility; i >= 0; i--) {
            if (RightTokens.storage.containsKey(this.name + "_" + i)) {
                key = name + "_" + Integer.toString(i);
                break;
            }
        }

        if (RightTokens.storage.containsKey(key)) {
            RightTokens.storage.remove(key);

        }
        RightTokens.storage.put(key, this);

        this.name = name;
    }


    public String calculateValue(ArrayList<Token> tokens) {
        for (int i = 0; i < tokens.size(); ++i) {
            Token token = tokens.get(i);
            if (token.type.equals(LexemeType.VAR.name())) {
                Variable var = Variable.get(token.value);
                Token new_token = new Token(LexemeType.DIGIT.name(), var.value, 0, 0);
                tokens.set(i, new_token);
            }
        }

        ReversePolishEntry rpe = new ReversePolishEntry(tokens);
        return rpe.toRPE();
    }

    public static void changeVisibility(Boolean inside) {
        if (inside) {
            Variable.visibility++;
        } else {
            Variable.visibility--;
        }
    }

    public static Variable get(String name) {
        for (int i = Variable.visibility; i >= 0; i--) {
            if (RightTokens.storage.containsKey(name + "_" + i)) {
                return RightTokens.storage.get(name + "_" + i);
            }
        }
        throw new ExceptionInInitializerError("Undefined variable " + name);
    }


}
