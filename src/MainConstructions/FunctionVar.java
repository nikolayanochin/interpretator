package MainConstructions;

import Lexer.LexemeType;
import Lexer.RightTokens;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 29.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class FunctionVar {

    private String value = "";

    public FunctionVar(String function) {
        Pattern pattern = LexemeType.VAR.getPattern();

        Matcher matcher = pattern.matcher(function);
        while (matcher.find()) {
            String var_name = matcher.group();
            Variable var = Variable.get(var_name);
            this.value = var.value;
        }


        pattern = LexemeType.VAR.getPattern();

        matcher = pattern.matcher(function);
        while (matcher.find()) {
            String var_name = matcher.group();
            Variable var = Variable.get(var_name);
            this.value = var.value;
        }
        this.logic();
    }

    private void logic() {
        System.out.println(this.value);
    }
}
