package MainConstructions;

import Lexer.LexemeType;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 31.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class Function {
    private String value = "";
    private String function;
    private ArrayList<String> function_args = new ArrayList<>();
    public String result;


    public Function(String function) {
        this.function = function;
        Pattern pattern = Pattern.compile("[^(][$A-z_0-9, \"]*[^)]");
        Matcher matcher = pattern.matcher(function);

        String function_head = function;
        while (matcher.find()) {
            function_head = matcher.group();
        }


        String[] matching = function_head.split("\\s*,\\s*", 3);
        for (int i = 0; i < matching.length; i++) {
            String function_args = matching[i];

            pattern = LexemeType.VAR.getPattern();
            matcher = pattern.matcher(function_args);
            if (matcher.find()) {
                String var_name = matcher.group();
                Variable var = Variable.get(var_name);
                this.function_args.add(var.name);
            } else {
                function_args = function_args.replaceAll("\"", "");
                this.function_args.add(function_args);
            }
        }


        logic();

    }

    private void logic() {
        Pattern pattern = Pattern.compile("[A-z]*");
        Matcher matcher = pattern.matcher(this.function);
        String function_name = "";
        if (matcher.find()) {
            function_name = matcher.group();
        }

        Variable var = Variable.get(this.function_args.get(0));

        if (function_name.equals("add")) {
            var.hashMap.put(this.function_args.get(1), this.function_args.get(2));
        }

        if (function_name.equals("remove")) {
            var.hashMap.remove(this.function_args.get(1));
        }

        if (function_name.equals("exists")) {
            if(var.hashMap.exists(this.function_args.get(1))) {
                this.result = "1";
            } else {
                this.result = "0";
            }
        }
    }
}
