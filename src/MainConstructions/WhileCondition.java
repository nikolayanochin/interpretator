package MainConstructions;

import Lexer.LexemeType;
import Parser.ControlStructureInterface;
import Parser.Parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 30.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class WhileCondition extends ControlStructureInterface {
    public Boolean condition = false;
    public Integer start_line;
    public Integer end_line;
    public String expression;

    public WhileCondition(String expression, Integer start_line) {
        this.start_line = start_line;
        this.expression = expression;
        setCondition();
    }

    private Boolean setCondition() {
        Pattern pattern_condition = LexemeType.CONDITION.getPattern();
        Matcher matcher = pattern_condition.matcher(this.expression);
        while (matcher.find()) {
            String value = matcher.group();
            Condition condition = new Condition(value);
            this.condition = condition.result;
        }
        return this.condition;
    }

    @Override
    public void logic() {
        Parser parser = new Parser();
        while (this.condition) {
            if (!this.setCondition()) {
                break;
            }
            // while line 3, 4
            Integer while_line = this.start_line + 1;
            parser.line = while_line;
            while (while_line <= this.end_line - 1) {
                String code = Parser.allCode.get(while_line - 1);
                parser.setCode(code);
                parser.parse(false);
                while_line++;
            }
        }
    }

    @Override
    public void setEndLine(Integer line) {
        this.end_line = line;
    }
}
