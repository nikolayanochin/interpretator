package Parser;

import Lexer.LexemeType;
import Lexer.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.IntStream;

/**
 * Created by Anochin Nikolay on 29.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class ReversePolishEntry {
    ArrayList<Token> inputTokens;
    ArrayList<Token> stack = new ArrayList<>();
    ArrayList<Double> output = new ArrayList<>();


    public ReversePolishEntry(ArrayList<Token> tokens) {
        this.inputTokens = tokens;
    }

    private void checkGoOut(Token token) {
        if (this.stack.size() > 0) {
            Token prev_token = this.stack.get(this.stack.size() - 1);
            Integer a = getTokenPriority(token);
            Integer b = getTokenPriority(prev_token);
            if (getTokenPriority(token) - getTokenPriority(prev_token) <= 0) {
                this.stack.remove(this.stack.size() - 1);
                goOut(prev_token.value);
            }
        }
    }

    private void goOut(String operand) {

        Double right_value = this.output.get(this.output.size() - 1);
        this.output.remove(this.output.size() - 1);
        Double left_value = this.output.get(this.output.size() - 1);
        this.output.remove(this.output.size() - 1);
        Double value = ReversePolishEntry.calculate(left_value, operand, right_value);
        this.output.add(value);
    }

    private void goOutAll() {
        while (this.stack.size() > 0) {
            Token token = this.stack.get(this.stack.size() - 1);

            this.stack.remove(this.stack.size() - 1);
            if (!token.type.equals(LexemeType.LEFT_BRACKET.name()) && !token.type.equals(LexemeType.RIGHT_BRACKET.name())) {
                goOut(token.value);
            }
        }
    }

    public String toRPE() {
        ArrayList<Token> tokens = this.inputTokens;
        for (int i = 0; i < tokens.size(); ++i) {
            Token token = tokens.get(i);

            if (token.type.equals(LexemeType.DIGIT.name())) {
                this.output.add(Double.parseDouble(token.value));
                if (i + 1 == tokens.size()) {
                    this.goOutAll();
                }
            } else if (token.type.equals(LexemeType.OP.name())) {
                this.checkGoOut(token);
                this.stack.add(token);
            } else if (token.type.equals(LexemeType.LEFT_BRACKET.name())) {
                this.stack.add(token);
            } else if (token.type.equals(LexemeType.RIGHT_BRACKET.name())) {
                int[] start_positions = new int[stack.size()];
                int[] result = Arrays.copyOf(start_positions, start_positions.length + 1);
                for (int c = stack.size() - 1; c > 0; c--) {
                    Token thisToken = this.stack.get(c);
                    result[result.length - c] = thisToken.start_position;
                    if (thisToken.type.equals(LexemeType.LEFT_BRACKET.name())) {
                        break;
                    }
                }

                for (Iterator<Token> iterator = this.stack.iterator(); iterator.hasNext(); ) {
                    Token thisToken = iterator.next();
                    boolean contains = IntStream.of(result).anyMatch(x -> x == thisToken.start_position);
                    if (contains) {
                        if (thisToken.type.equals(LexemeType.LEFT_BRACKET.name())) {
                            iterator.remove();
                        } else {
                            iterator.remove();
                            this.goOut(thisToken.value);
                        }
                    }
                }

            }
        }
        return toString(this.output.get(0));
    }


    private Integer getTokenPriority(Token token) {
        switch (token.value) {
            case "*":
            case "/":
                return 2;
            case "+":
            case "-":
                return 1;
            default:
                return 0;
        }
    }


    public static String toString(Double i) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append(i);
        String string = stringBuilder.toString();
        return string;
    }

    public static Double calculate(Double left_value, String operation, Double right_value) {
        Double value = 0.0;

        switch (operation) {
            case "+":
                value = left_value + right_value;
                break;
            case "-":
                value = left_value - right_value;
                break;
            case "*":
                value = left_value * right_value;
                break;
            case "/":
                value = left_value / right_value;
                break;
        }

        return value;
    }
}
