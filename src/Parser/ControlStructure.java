package Parser;

/**
 * Created by Anochin Nikolay on 30.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class ControlStructure extends ControlStructureInterface {

    private ControlStructureInterface condition;

    ControlStructure(ControlStructureInterface condition) {
        this.condition = condition;
    }

    @Override
    public void logic() {
        this.condition.logic();
    }

    @Override
    public void setEndLine(Integer line) {
        this.condition.setEndLine(line);
    }
}
