package Parser;

import Lexer.LexemeType;
import Lexer.RightTokens;
import Lexer.Token;
import MainConstructions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 28.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */

public class Parser {
    private String inputString;
    private String code;
    private ArrayList<Token> tokens = new ArrayList<>();
    private ArrayList<LexemeType> notFoundedLexemes = new ArrayList<>();
    private Boolean isOtherVisibility = false;
    public Integer line = 1;
    private static final String RED = "\033[0;31m";
    public static ArrayList<String> allCode = new ArrayList<>();
    private static ArrayList<ControlStructureInterface> controlStructures = new ArrayList<>();
    private Boolean isIfFalseCondition = false;

    public void setCode(String str) {
        this.code = str;
    }

    /**
     * Разбиваем нашу строку на токены
     *
     * @return ArrayList<Lexer.Token>
     */
    public String parse(Boolean doc_code) {
        this.tokens = new ArrayList<>();
        this.notFoundedLexemes = new ArrayList<>();
        try {
            this.inputString = this.code;
            if (doc_code) {
                allCode.add(this.inputString);
            }
            Boolean founded = false;

            if (this.isOtherVisibility) {
                Pattern pattern = LexemeType.RIGHT_CURVED_BRACKET.getPattern();
                Matcher m = pattern.matcher(this.code);

                while (m.find()) {
                    ControlStructureInterface structure = Parser.controlStructures.get(controlStructures.size() - 1);
                    Parser.controlStructures.remove(controlStructures.size() - 1);
                    structure.setEndLine(this.line);
                    structure.logic();
                    founded = true;
                    this.isIfFalseCondition = false;
                }

                if (this.isIfFalseCondition) {
                    this.nextString();
                    return "";
                }
            }

            this.setTokens();
            // Удаляем все пробелы и табы из кода TODO если введешь тип стринг исправь!
            this.code = this.code.replaceAll("[\\t ]", "");

            // Сортируем наши токены по порядку
            Collections.sort(this.tokens, Token.COMPARE_BY_START);

            if (founded) {
                Variable.changeVisibility(false);
            }

            this.checkSyntacsis();

            if (founded && Variable.visibility.equals(0)) {
                this.isOtherVisibility = false;
            }
            this.setConstructions();

            this.nextString();

        } catch (ExceptionInInitializerError e) {
            this.outError(e.getMessage());
        }
        return "";
    }


    private void setTokens() {
        if (!this.isIfFalseCondition) {
            for (LexemeType lexeme : LexemeType.values()) {
                Pattern pattern = lexeme.getPattern();
                Matcher m = pattern.matcher(this.code);
                String type = lexeme.name();

                Integer lexemes_count = 0;
                while (m.find()) {
                    Integer index_lexeme = m.start();
                    String value = m.group();
                    if (!lexeme.name().equals(LexemeType.CODE_COMMENT.name())) {
                        Token token = new Token(type, value, index_lexeme, m.end());
                        this.tokens.add(token);
                    }
                    lexemes_count++;

                    // Древний костыль заменяющий найденные лексемы на #, чтобы не терять позицию TODO ? ? ?
                    char[] chArr = this.code.toCharArray();
                    for (int i = m.start(); i < m.end(); i++) {
                        chArr[i] = '#';
                    }

                    this.code = new String(chArr);
                }

                if (lexemes_count == 0) {
                    this.notFoundedLexemes.add(lexeme);
                }
            }
        }
    }

    private void nextString() {
        this.code = "";
        this.inputString = "";
        this.tokens = new ArrayList<>();
        this.notFoundedLexemes = new ArrayList<>();
        this.line++;
    }

    private void outError(String message) {
        System.out.println(Parser.RED + "Ошибка в строке " + this.line + ".");
        System.out.println(Parser.RED + message);
        System.exit(1);
    }

    private void checkSyntacsis() {
        if (!RightTokens.checkTokens(this.tokens, this.notFoundedLexemes)) {
            this.outError("Unexpected token " + this.inputString);
        }
        Pattern pattern = Pattern.compile("[^#]");
        Matcher m = pattern.matcher(this.code);
        while (m.find()) {
            String value = m.group();
            this.outError("Unexpected token " + value);
        }
    }


    private void setConstructions() {
        String result = "";
        for (Token token : this.tokens) {
            if (token.type.equals(LexemeType.FUNCTION_WITH_VAR.name())) {
                new FunctionVar(token.value);
            } if (token.type.equals(LexemeType.FUNCTION_WITH_LONG_VARS.name())) {
                Function f = new Function(token.value);
                result = f.result;
            } else if (token.type.equals(LexemeType.IF.name())) {
                IfCondition ifCondition = new IfCondition(token.value, this.line);
                Parser.controlStructures.add(new ControlStructure(ifCondition));
                if (!ifCondition.condition) {
                    Variable.changeVisibility(true);
                    this.isIfFalseCondition = true;
                } else {
                    Variable.changeVisibility(true);
                }
                this.isOtherVisibility = true;
            } else if (token.type.equals(LexemeType.WHILE.name())) {
                WhileCondition whileCondition = new WhileCondition(token.value, this.line);
                ControlStructure controlStructure = new ControlStructure(whileCondition);
                Parser.controlStructures.add(controlStructure);
                if (!whileCondition.condition) {
                    Variable.changeVisibility(true);
                } else {
                    Variable.changeVisibility(true);
                }
                this.isOtherVisibility = true;
            }
        }

        if (this.tokens.size() > 1)

        {
            Token token = this.tokens.get(1);
            if (token.type.equals(LexemeType.VAR.name())) {
                Token operand = this.tokens.get(2);
                if (operand.type.equals(LexemeType.ASSIGN_OP.name())) {
                    ArrayList<Token> cutTokens = tokens;
                    cutTokens.remove(0);
                    cutTokens.remove(0);
                    cutTokens.remove(0);
                    cutTokens.remove(cutTokens.size() - 1);
                    if(result.length() > 0)  {
                        cutTokens.remove(0);
                        Token res_token = new Token(LexemeType.DIGIT.name(), result, 0, 0);
                        cutTokens.add(res_token);
                    }
                    new Variable(token.value, cutTokens);
                }
            }
        }
    }

}
