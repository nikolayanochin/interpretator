package Parser;

/**
 * Created by Anochin Nikolay on 30.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public abstract class ControlStructureInterface {
    public abstract void logic();
    public abstract void setEndLine(Integer line);
}
