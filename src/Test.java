import HashMap.MyHashMap;
import HashMap.MyLinkedList;
import Parser.Parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Anochin Nikolay on 28.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class Test {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(new File("code.txt").getAbsolutePath()));
        String codeLine;
        Parser parser = new Parser();
        while(sc.hasNext()){
            codeLine = sc.nextLine();
            parser.setCode(codeLine);
            parser.parse(true);
        }

//        MyLinkedList<Integer> list = new MyLinkedList<>();
//        MyHashMap<String> a = new MyHashMap<String>();
//        a.put("key", "string");
//        //a.remove("key");
//        a.exists("key");
//        a.put("key1234", "string");
    }
}
