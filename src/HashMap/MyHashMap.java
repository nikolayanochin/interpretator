package HashMap;

/**
 * Created by Anochin Nikolay on 31.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class MyHashMap<E> {
    private MyLinkedList<Entity> table[];
    private int size = 16;

    @SuppressWarnings("unchecked")
    public MyHashMap() {
        this.table = new MyLinkedList[this.size];
    }


    public void put(String key, E object) {
        Entity<E> entity = new Entity<>(key, object);

        int index = this.getIndex(key);
        if (this.table[index] == null) {
            this.table[index] = new MyLinkedList<>();
        }
        this.table[index].add(entity);
    }

    private int hashCode(String key) {
        return (int) key.charAt(0);
    }

    private int getIndex(String key) {
        return this.hashCode(key) & (this.size - 1);
    }


    private static class Entity<E> {
        private String key;
        private transient E element;

        Entity(String key, E element) {
            this.key = key;
            this.element = element;
        }

        @Override
        public int hashCode() {
            return (int) key.charAt(0);
        }

        @Override
        public boolean equals(Object obj) {
            return key.equals((String) obj);
        }
    }


    public void remove(String key) {
        int index = this.getIndex(key);
        if (this.table[index] != null) {

           this.table[index].remove(key);
        }
    }

    public boolean exists(String key) {
        int index = this.getIndex(key);
        if (this.table[index] == null) {
            return false;
        }

        Entity entity = this.table[index].findByHash(key);

        if (entity != null) {
            return true;
        }
        return false;
    }

}
