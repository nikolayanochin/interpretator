package HashMap;

/**
 * Created by Anochin Nikolay on 31.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class MyLinkedList<E> {
    private transient int size = 0;
    private transient MyNode<E> first = null;
    private transient MyNode<E> last = null;

    public void add(Integer index, E element) {
        if (index > this.size) {
            throw new IndexOutOfBoundsException();
        }

        if (index == this.size) {
            this.add(element);
        }

        if (index < this.size) {
            if (index < 0) {
                throw new IndexOutOfBoundsException();
            }

            MyNode<E> prevNode = this.findNode(index);
            MyNode<E> nextNode = this.findNode(index + 1);
            MyNode<E> node = new MyNode<>(prevNode, element, nextNode);
            prevNode.next_el = node;

            if (nextNode != null) {
                nextNode.prev_el = node;
            } else {
                this.last = node;
            }

            this.size++;
        }
    }

    public void add(E element) {
        MyNode<E> node = new MyNode<>(this.last, element, null);
        if (this.first != null) {
            this.last.next_el = node;
            this.last = node;
        } else {
            this.first = node;
            this.last = node;
        }
        this.size++;
    }

    private MyNode<E> findNode(Integer index) {
        MyNode<E> thisNode = this.first;
        for (int i = 0; i < index; i++) {
            thisNode = thisNode.next_el;
        }
        return thisNode;
    }

    public void remove(Integer index) {
        if (index == 1) {
            if (this.size > 1) {
                MyNode<E> next_node = this.first.next_el;
                next_node.prev_el = null;
                this.first = next_node;
            } else {
                this.first = null;
                this.last = null;
            }
        } else if (index == this.size) {
            this.last.prev_el.next_el = null;
            this.last = this.last.prev_el;
        } else {
            MyNode<E> prev_node = this.findNode(index - 2);
            MyNode<E> next_node = this.findNode(index);
            prev_node.next_el = next_node;
            next_node.prev_el = prev_node;
        }

        this.size--;
    }

    public void remove(String key) {
        MyNode<E> thisNode = this.first;
        for (int i = 0; i < this.size; i++) {
            if (thisNode.element.equals(key)) {
                this.remove(i);
            }
        }
    }

    public boolean exists(Integer index) {
        if (index < this.size) {
            MyNode<E> node = this.findNode(index - 1);
            if (node != null) {
                return true;
            }
        }
        return false;
    }

    public E findByHash(String key) {
        MyNode<E> thisNode = this.first;
        E finded = null;
        for (int i = 0; i < this.size; i++) {
            if (thisNode.element.equals(key)) {
                finded = thisNode.element;
            }
        }
        return finded;
    }

    private static class MyNode<E> {
        E element;
        MyNode<E> next_el;
        MyNode<E> prev_el;

        MyNode(MyNode<E> prev_el, E element, MyNode<E> next_el) {
            this.element = element;
            this.next_el = next_el;
            this.prev_el = prev_el;
        }
    }

}
