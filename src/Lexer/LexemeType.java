package Lexer;

import java.util.regex.Pattern;

/**
 * Created by Anochin Nikolay on 28.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public enum LexemeType {

    START_STRING,
    CODE_COMMENT,
    IF,
    WHILE,
    FUNCTION,
    FUNCTION_WITH_VAR,
    FUNCTION_WITH_LONG_VARS,
    CONDITION,
    VAR,
    ASSIGN_OP,
    DIGIT,
    OP,
    LEFT_BRACKET,
    RIGHT_BRACKET,
    LEFT_CURVED_BRACKET,
    END_STRING,
    RIGHT_CURVED_BRACKET;


    public Pattern getPattern() {
        switch (this) {
            case DIGIT:
                return Pattern.compile("0|[1-9][0-9]*");
            case OP:
                return Pattern.compile("\\+|-|\\*|/");
            case VAR:
                return Pattern.compile("[$][A-z][A-z_0-9]*");
            case ASSIGN_OP:
                return Pattern.compile("=");
            case LEFT_BRACKET:
                return Pattern.compile("\\(");
            case RIGHT_BRACKET:
                return Pattern.compile("\\)");
            case RIGHT_CURVED_BRACKET:
                return Pattern.compile("}");
            case LEFT_CURVED_BRACKET:
                return Pattern.compile("\\{");
            case FUNCTION:
                return Pattern.compile("[A-z][A-z_0-9]+[(][)]");
            case FUNCTION_WITH_VAR:
                return Pattern.compile("[A-z][A-z_0-9]+[(]" + LexemeType.VAR.getPattern().toString() + "[)]");
            case FUNCTION_WITH_LONG_VARS:
                return Pattern.compile("[A-z][A-z_0-9]+[(][$A-z_0-9, \"]*[)]");
            case CONDITION:
                return Pattern.compile("[$A-z 0-9]*(==|<=|>=|!=|>|<)[$A-z 0-9]*");
            case IF:
                return Pattern.compile("if[ ]*\\(" + LexemeType.CONDITION.getPattern().toString() + "\\)[ ]*");
            case WHILE:
                return Pattern.compile("while[ ]*\\(" + LexemeType.CONDITION.getPattern().toString() + "\\)[ ]*");
            case START_STRING:
                return Pattern.compile("^");
            case END_STRING:
                return Pattern.compile("$");
            case CODE_COMMENT:
                return Pattern.compile("#(.*)");
            default:
                throw new AssertionError("Unknown operations " + this);
        }
    }
}
