package Lexer;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Anochin Nikolay on 28.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class Token implements Serializable {

    public String type;
    public String value;

    public Integer start_position;
    public Integer end_position;

    public Token(String type, String value, Integer start_position, Integer end_position) {
        this.start_position = start_position;
        this.end_position = end_position;
        this.type = type;
        this.value = value;
    }

    public static final Comparator<Token> COMPARE_BY_START = new Comparator<Token>() {
        @Override
        public int compare(Token lhs, Token rhs) {
            return lhs.start_position - rhs.start_position;
        }
    };

    /**
     *
     * @return Lexeme
     */
    public LexemeType getLexemeByName() {
        for (LexemeType lexeme : LexemeType.values()) {
            if (lexeme.name().equals(this.type)) {
                return lexeme;
            }
        }
        throw new AssertionError("Unknown lexeme " + this.type);
    }
}
