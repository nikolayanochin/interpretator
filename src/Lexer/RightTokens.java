package Lexer;

import MainConstructions.Variable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anochin Nikolay on 29.05.2018.
 * Email: titan12345@mail.ru
 * Telephone: +7 925 174 3 14
 */
public class RightTokens {

    public static Map<String, Variable> storage = new HashMap<>();
    private static Integer right_curved_braket_count = 0;
    private static Integer left_curved_braket_count = 0;

    private static ArrayList<Enum> rightEnumChain(Token token) {
        ArrayList<Enum> chain = new ArrayList<>();

        LexemeType lexeme = token.getLexemeByName();
        switch (lexeme) {
            case DIGIT:
                chain.add(LexemeType.OP);
                chain.add(LexemeType.RIGHT_BRACKET);
                chain.add(LexemeType.END_STRING);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case OP:
                chain.add(LexemeType.VAR);
                chain.add(LexemeType.DIGIT);
                chain.add(LexemeType.LEFT_BRACKET);
                chain.add(LexemeType.RIGHT_BRACKET);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case VAR:
                chain.add(LexemeType.OP);
                chain.add(LexemeType.ASSIGN_OP);
                chain.add(LexemeType.END_STRING);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case ASSIGN_OP:
                chain.add(LexemeType.VAR);
                chain.add(LexemeType.DIGIT);
                chain.add(LexemeType.LEFT_BRACKET);
                chain.add(LexemeType.CODE_COMMENT);
                chain.add(LexemeType.FUNCTION);
                chain.add(LexemeType.FUNCTION_WITH_LONG_VARS);
                break;
            case LEFT_BRACKET:
                chain.add(LexemeType.VAR);
                chain.add(LexemeType.DIGIT);
                chain.add(LexemeType.LEFT_BRACKET);
                chain.add(LexemeType.CONDITION);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case RIGHT_BRACKET:
                chain.add(LexemeType.OP);
                chain.add(LexemeType.RIGHT_BRACKET);
                chain.add(LexemeType.LEFT_CURVED_BRACKET);
                chain.add(LexemeType.END_STRING);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case CONDITION:
                chain.add(LexemeType.RIGHT_BRACKET);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case IF:
                chain.add(LexemeType.LEFT_CURVED_BRACKET);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case WHILE:
                chain.add(LexemeType.LEFT_CURVED_BRACKET);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case LEFT_CURVED_BRACKET:
                chain.add(LexemeType.END_STRING);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case START_STRING:
                chain.add(LexemeType.VAR);
                chain.add(LexemeType.IF);
                chain.add(LexemeType.WHILE);
                chain.add(LexemeType.RIGHT_CURVED_BRACKET);
                chain.add(LexemeType.FUNCTION_WITH_VAR);
                chain.add(LexemeType.END_STRING);
                chain.add(LexemeType.CODE_COMMENT);
                chain.add(LexemeType.FUNCTION_WITH_LONG_VARS);
                break;
            case FUNCTION_WITH_VAR:
            case FUNCTION_WITH_LONG_VARS:
            case FUNCTION:
            case RIGHT_CURVED_BRACKET:
                chain.add(LexemeType.END_STRING);
                chain.add(LexemeType.CODE_COMMENT);
                break;
            case CODE_COMMENT:
                chain.add(LexemeType.END_STRING);
                break;
        }

        return chain;
    }

    /**
     * Чего не может быть без этих лексем
     *
     * @param lexeme LexemeType
     * @return ArrayList<Enum>
     */
    private static ArrayList<Enum> notBeChain(LexemeType lexeme) {
        ArrayList<Enum> chain = new ArrayList<>();

        switch (lexeme) {
            case VAR:
                chain.add(LexemeType.ASSIGN_OP);
                chain.add(LexemeType.DIGIT);
                chain.add(LexemeType.OP);
                break;
            case ASSIGN_OP:
                chain.add(LexemeType.VAR);
                chain.add(LexemeType.DIGIT);
                chain.add(LexemeType.OP);
                break;
        }

        return chain;
    }

    public static boolean checkTokens(ArrayList<Token> tokens, ArrayList<LexemeType> notFoundedLexemes) {
        boolean condition = true;
        Integer right_braket_count = 0;
        Integer left_braket_count = 0;
        // Проверяем, следующая лексема может следовать за предыдущей
        for (int i = 0; i < tokens.size(); ++i) {
            Token token = tokens.get(i);

            if (token.type.equals(LexemeType.RIGHT_BRACKET.name())) {
                right_braket_count++;
            } else if (token.type.equals(LexemeType.LEFT_BRACKET.name())) {
                left_braket_count++;
            }

            if (token.type.equals(LexemeType.RIGHT_CURVED_BRACKET.name())) {
                RightTokens.right_curved_braket_count++;
            }

            if (token.type.equals(LexemeType.LEFT_CURVED_BRACKET.name())) {
                RightTokens.left_curved_braket_count++;
            }

            if (RightTokens.right_curved_braket_count > RightTokens.left_curved_braket_count) {
                return false;
            }

            ArrayList<Enum> rightEnums = rightEnumChain(token);

            if (i + 1 < tokens.size()) {
                Integer count = 0;
                for (Enum LexemeType : rightEnums) {
                    Token next_token = tokens.get(i + 1);
                    if (LexemeType.name().equals(next_token.type)) {
                        count++;
                    }
                }
                if (count == 0) {
                    condition = false;
                    break;
                }
            } else {
                if (token.type.equals(LexemeType.ASSIGN_OP.name())) {
                    condition = false;
                    break;
                }
            }
        }

        if (!right_braket_count.equals(left_braket_count)) {
            return false;
        }

        // Если в строке нет какой-то лексемы, проверяем, могут ли все токены использоваться
        for (LexemeType lexeme : notFoundedLexemes) {
            ArrayList<Enum> wrongEnums = notBeChain(lexeme);
            for (Token token : tokens) {
                for (Enum wrongEnum : wrongEnums) {
                    if (wrongEnum.name().equals(token.type)) {
                        condition &= false;
                        break;
                    }
                }
                if (!condition) {
                    break;
                }
            }
        }

        return condition;
    }
}
